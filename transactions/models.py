from django.db import models

class Sale(models.Model):
    class Meta:
        db_table = 'transactions_Ventas'

    def __str__(self):
        return "Sale #{}".format(self.id)

    client = models.ForeignKey('clients.Client',
                   related_name='sales',
                   on_delete=models.CASCADE)
    fecha = models.DateField()
    fecha.db_column = 'Fecha'

    tipo_comprobante = models.CharField(max_length=20, null=True, blank=True)
    tipo_comprobante.db_column = 'Tipo_Comprobante'

    comprobante_nombre = models.CharField(max_length=100, null=True, blank=True)
    comprobante_nombre.db_column = 'Comprobante_Nombre'

    id_comprobante = models.CharField(max_length=20, null=True, blank=True)
    id_comprobante.db_column = 'Id_Comprobante'    

    numero_comprobante = models.CharField(max_length=20, null=True, blank=True)
    numero_comprobante.db_column = 'Numero_Comprobante'

    tipo_operacion = models.CharField(max_length=20, null=True, blank=True)
    tipo_operacion.db_column = 'Tipo_De_Operacion'

    tipo_operacion_2 = models.CharField(max_length=20, null=True, blank=True)
    tipo_operacion_2.db_column = 'Tipo_De_Operacion_2'

    moneda = models.CharField(max_length=20, null=True, blank=True)
    moneda.db_column = 'Moneda'
    cotizacion_tc = models.CharField(max_length=20, null=True, blank=True)
    cotizacion_tc.db_column = 'Cotizacion_Tc'
    condicion_de_pago_codigo = models.CharField(max_length=20, null=True, blank=True)
    condicion_de_pago_codigo.db_column = 'Condicion_De_Pago_Codigo'
    condicion_de_pago_nombre = models.CharField(max_length=20, null=True, blank=True)
    condicion_de_pago_nombre.db_column = 'Condicion_De_Pago_Nombre'
    precio_unitario = models.FloatField(null=True, blank=True)
    precio_unitario.db_column = 'Precio_Unitario'
    subtotal = models.FloatField(null=True, blank=True)
    subtotal.db_column = 'Subtotal'
    descuento = models.FloatField(null=True, blank=True)
    descuento.db_column = 'Descuento'
    descuento_1 = models.FloatField(null=True, blank=True)
    descuento_1.db_column = 'Descuento1'
    descuento_2 = models.FloatField(null=True, blank=True)
    descuento_2.db_column = 'Descuento2' 
    descuento_3 = models.FloatField(null=True, blank=True)
    descuento_3.db_column = 'Descuento3'
    iva = models.FloatField(null=True, blank=True)
    iva.db_column = 'Iva'
    redondeo = models.FloatField(null=True, blank=True)
    redondeo.db_column = 'Redondeo'
    total = models.FloatField(null=True, blank=True)
    total.db_column = 'Total'
    cant_vendida = models.FloatField(null=True, blank=True)
    cant_vendida.db_column = 'Cant_Vendida'
    unidades = models.CharField(max_length=20, null=True, blank=True)
    unidades.db_column = 'Unidades'
    lote_numero = models.CharField(max_length=20, null=True, blank=True)
    lote_numero.db_column = 'Lote_N'
    lote_vencimiento = models.CharField(max_length=20, null=True, blank=True)
    lote_vencimiento.db_column = 'Lote_Vencimiento'
    familia = models.CharField(max_length=100, null=True, blank=True)
    familia.db_column = 'Familia'
    familia_numero = models.CharField(max_length=20, null=True, blank=True)
    familia_numero.db_column = 'Familia_N' 
    categoria = models.CharField(max_length=100, null=True, blank=True)
    categoria.db_column = 'Categoria'
    categoria_numero = models.CharField(max_length=20, null=True, blank=True)
    categoria_numero.db_column = 'Categoria_N'
    marca = models.CharField(max_length=100, null=True, blank=True)
    marca.db_column = 'Marca'
    marca_numero = models.CharField(max_length=20, null=True, blank=True)
    marca_numero.db_column = 'Marca_N'
    producto = models.CharField(max_length=100, null=True, blank=True)
    producto.db_column = 'Producto'
    producto_numero = models.CharField(max_length=20, null=True, blank=True)
    producto_numero.db_column = 'Producto_N'
    otros_detalles = models.CharField(max_length=20, null=True, blank=True)
    otros_detalles.db_column = 'Otros_Detalles'
    deposito_numero = models.CharField(max_length=20, null=True, blank=True)
    deposito_numero.db_column = 'Deposito_N'
    deposito_nombre = models.CharField(max_length=100, null=True, blank=True)
    deposito_nombre.db_column = 'Deposito_Nombre'
    proveedor_numero = models.CharField(max_length=20, null=True, blank=True)
    proveedor_numero.db_column = 'Proveedor_N'
    proveedor = models.CharField(max_length=100, null=True, blank=True)
    proveedor.db_column = 'Proveedor'
    local_nombre = models.CharField(max_length=100, null=True, blank=True)
    local_nombre.db_column = 'Local_Nombre'
    departamento = models.CharField(max_length=100, null=True, blank=True)
    departamento.db_column = 'Departamento'
    departamento_numero = models.CharField(max_length=20, null=True, blank=True)
    departamento_numero.db_column = 'Departamento_N'
    zona = models.CharField(max_length=20, null=True, blank=True)
    zona.db_column = 'Zona'
    zona_numero = models.CharField(max_length=100, null=True, blank=True)
    zona_numero.db_column = 'Zona_N'
    vendedor_numero = models.CharField(max_length=20, null=True, blank=True)
    vendedor_numero.db_column = 'N_Vendedor'
    vendedor = models.CharField(max_length=100, null=True, blank=True)
    vendedor.db_column = 'Vendedor'
    cliente_numero = models.CharField(max_length=20, null=True, blank=True)
    cliente_numero.db_column = 'N_Cliente'
    nombre = models.CharField(max_length=100, null=True, blank=True)
    nombre.db_column = 'Nombre'
    tipo_clientes = models.CharField(max_length=20, null=True, blank=True)
    tipo_clientes.db_column = 'Tipo_Clientes'
    pais_destino = models.CharField(max_length=20, null=True, blank=True)
    pais_destino.db_column = 'Pais_Destino'
    codigo_pais = models.CharField(max_length=100, null=True, blank=True)
    codigo_pais.db_column = 'Codigo_Pais'
    causa = models.CharField(max_length=20, null=True, blank=True)
    causa.db_column = 'Causa'
    tipo_venta = models.CharField(max_length=20, null=True, blank=True)
    tipo_venta.db_column = 'Tipo_Venta'
    centro_de_costos_numero = models.CharField(max_length=20, null=True, blank=True)
    centro_de_costos_numero.db_column = 'Centro_De_Costos_N'
    centro_de_costos_nombre = models.CharField(max_length=100, null=True, blank=True)
    centro_de_costos_nombre.db_column = 'Centro_De_Costos_Nombre'


class CompraGasto(models.Model):
    class Meta:
        db_table = 'transactions_ComprasGastos'

    def __str__(self):
        return "Gasto #{}".format(self.id)

    client = models.ForeignKey('clients.Client',
                   related_name='comprasgastos',
                   on_delete=models.CASCADE)
    fecha = models.DateField()
    fecha.db_column = 'Fecha'
    corresponde_a_gasto = models.CharField(max_length=1, null=True, blank=True)
    corresponde_a_gasto.db_column = 'Corresponde_A_Gasto'
    factura_signo = models.FloatField(null=True, blank=True)
    factura_signo.db_column = 'Factura_Signo'
    tipo_comprobante = models.CharField(max_length=20, null=True, blank=True)
    tipo_comprobante.db_column = 'Tipo_Comprobante'
    tipo_de_comprobante_nombre = models.CharField(max_length=100, null=True, blank=True)
    tipo_de_comprobante_nombre.db_column = 'Tipo_De_Comprobante_Nombre'
    id_comprobante = models.IntegerField(null=True, blank=True)
    id_comprobante.db_column = 'Id_Comprobante'
    numero = models.CharField(max_length=20, null=True, blank=True)
    numero.db_column = 'Numero'
    tipo_de_operacion = models.CharField(max_length=20, null=True, blank=True)
    tipo_de_operacion.db_column = 'Tipo_De_Operacion'
    moneda = models.CharField(max_length=5, null=True, blank=True)
    moneda.db_column = 'Moneda'
    cotizacion_tc = models.CharField(max_length=20, null=True, blank=True)
    cotizacion_tc.db_column = 'Cotizacion_Tc'
    condicion_de_pago_codigo = models.CharField(max_length=20, null=True, blank=True)
    condicion_de_pago_codigo.db_column = 'Condicion_De_Pago_Codigo'
    condicion_de_pago_nombre = models.CharField(max_length=100, null=True, blank=True)
    condicion_de_pago_nombre.db_column = 'Condicion_De_Pago_Nombre'
    producto_codigo = models.CharField(max_length=20, null=True, blank=True)
    producto_codigo.db_column = 'Producto_Codigo'
    producto_nombre = models.CharField(max_length=100, null=True, blank=True)
    producto_nombre.db_column = 'Producto_Nombre'
    cantidad = models.FloatField(null=True, blank=True)
    cantidad.db_column = 'Cantidad'
    precio_unitario = models.FloatField(null=True, blank=True)
    precio_unitario.db_column = 'Precio_Unitario'
    subtotal = models.FloatField(null=True, blank=True)
    subtotal.db_column = 'Subtotal'
    descuento = models.FloatField(null=True, blank=True)
    descuento.db_column = 'Descuento'
    iva = models.FloatField(null=True, blank=True)
    iva.db_column = 'Iva'
    total = models.FloatField(null=True, blank=True)
    total.db_column = 'Total'
    proveedor_codigo = models.CharField(max_length=20, null=True, blank=True)
    proveedor_codigo.db_column = 'Proveedor_Codigo'
    proveedor_nombre = models.CharField(max_length=100, null=True, blank=True)
    proveedor_nombre.db_column = 'Proveedor_Nombre'
    familia_codigo = models.CharField(max_length=20, null=True, blank=True)
    familia_codigo.db_column = 'Familia_Codigo'
    familia_nombre = models.CharField(max_length=100, null=True, blank=True)
    familia_nombre.db_column = 'Familia_Nombre'
    categoria_codigo = models.CharField(max_length=20, null=True, blank=True)
    categoria_codigo.db_column = 'Categoria_Codigo'
    categoria_nombre = models.CharField(max_length=100, null=True, blank=True)
    categoria_nombre.db_column = 'Categoria_Nombre'
    marca_codigo = models.CharField(max_length=20, null=True, blank=True)
    marca_codigo.db_column = 'Marca_Codigo'
    marca_nombre = models.CharField(max_length=100, null=True, blank=True)
    marca_nombre.db_column = 'Marca_Nombre'
    otros_detalles = models.CharField(max_length=100, null=True, blank=True)
    otros_detalles.db_column = 'Otros_Detalles'
    deposito_origen = models.CharField(max_length=100, null=True, blank=True)
    deposito_origen.db_column = 'Deposito_Origen'
    deposito_destino = models.CharField(max_length=100, null=True, blank=True)
    deposito_destino.db_column = 'Deposito_Destino'
    gastos_importacion = models.FloatField(null=True, blank=True)
    gastos_importacion.db_column = 'Gastos_Importacion'
    flete = models.FloatField(null=True, blank=True)
    flete.db_column = 'Flete'
    centro_de_costos_codigo = models.CharField(max_length=20, null=True, blank=True)
    centro_de_costos_codigo.db_column = 'Centro_De_Costos_Codigo'
    centro_de_costos_nombre = models.CharField(max_length=100, null=True, blank=True)
    centro_de_costos_nombre.db_column = 'Centro_De_Costos_Nombre'
    pais_de_compra_codigo = models.CharField(max_length=20, null=True, blank=True)
    pais_de_compra_codigo.db_column = 'Pais_De_Compra_Codigo'
    pais_de_compra_nombre = models.CharField(max_length=100, null=True, blank=True)
    pais_de_compra_nombre.db_column = 'Pais_De_Compra_Nombre'
    departamento_codigo = models.CharField(max_length=20, null=True, blank=True)
    departamento_codigo.db_column = 'Departamento_Codigo'
    departamento_nombre = models.CharField(max_length=100, null=True, blank=True)
    departamento_nombre.db_column = 'Departamento_Nombre'
    local_codigo = models.CharField(max_length=20, null=True, blank=True)
    local_codigo.db_column = 'Local_Codigo'
    local_nombre = models.CharField(max_length=100, null=True, blank=True)
    local_nombre.db_column = 'Local_Nombre'
    rubro = models.CharField(max_length=20, null=True, blank=True)
    rubro.db_column = 'Rubro'
    rubro_nombre =  models.CharField(max_length=100, null=True, blank=True)
    rubro_nombre.db_column = 'Rubro_Nombre'
    descripcion =  models.CharField(max_length=100, null=True, blank=True)
    descripcion.db_column = 'Descripcion'

