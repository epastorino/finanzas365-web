from django.conf.urls import url, include
from rest_framework import routers
from . import views


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^sales/', views.SaleList.as_view(), name='sales-list'),
    url(r'^comprasgastos/', views.CompraGastoList.as_view(), name='comprasgastos-list'),
]