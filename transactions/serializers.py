from rest_framework import serializers
from .models import Sale, CompraGasto

class SaleSerializer(serializers.ModelSerializer):
	class Meta:
		model = Sale
		exclude = ('client','id',)


class CompraGastoSerializer(serializers.ModelSerializer):
	class Meta:
		model = CompraGasto
		exclude = ('client','id',)

