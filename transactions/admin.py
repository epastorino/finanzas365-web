from django.contrib import admin

# Register your models here.
from .models import Sale, CompraGasto

admin.site.register(Sale)
admin.site.register(CompraGasto)