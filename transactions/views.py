from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from clients.models import Client

import uuid

from .models import Sale, CompraGasto
from .serializers import SaleSerializer, CompraGastoSerializer

# Create your views here.
class SaleList(generics.ListAPIView):
    serializer_class = SaleSerializer
    queryset = ''

    def list(self, request):
        try:
            key = request.GET['key']
        except:
            return Response("{\"error\": \"Debe especificar una key\"}", status=400)
        try:
            uid = uuid.UUID(str(key))
        except:
            return Response("{\"error\": \"Formato incorrecto de key\"}", status=400)
        try:
            client = Client.objects.get(key=key)
        except:
            return Response("{\"error\": \"La key es invalida\"}", status=400)
        queryset = client.sales.all()
        serializer = SaleSerializer(queryset, many=True)
        return Response(serializer.data)


class CompraGastoList(generics.ListAPIView):
    serializer_class = CompraGastoSerializer
    queryset = ''
    def list(self, request):
        try:
            key = request.GET['key']
        except:
            return Response("{\"error\": \"Debe especificar una key\"}", status=400)
        try:
            uid = uuid.UUID(str(key))
        except:
            return Response("{\"error\": \"Formato incorrecto de key\"}", status=400)
        try:
            client = Client.objects.get(key=key)
        except:
            return Response("{\"error\": \"La key es invalida\"}", status=400)
        queryset = client.comprasgastos.all()
        serializer = CompraGastoSerializer(queryset, many=True)
        return Response(serializer.data)
