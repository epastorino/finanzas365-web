from django.test import TestCase, Client as DjangoClient
from clients.models import Client, ZetaClient
# Create your tests here.

class ClientTestCase(TestCase):
	def setUp(self):
		 c = Client.objects.create(username='test')
		 c.set_password('test123')
		 c.save()
		 z = ZetaClient.objects.create(empresa_id='zeta1', empresa_key='abc123', name='zeta', client=c)

	def test_clients_can_login(self):
		c = DjangoClient()
		response = c.login(username='test', password='test123')
		self.assertEqual(response, True)

	def test_client_has_zetainfo(self):
		z = ZetaClient.objects.get(name='zeta')
		self.assertEqual(z.client.username, 'test')

