from django.db import models
from django.contrib.auth.models import User

from uuid import uuid4

# Create your models here.
class Client(User):
    key = models.CharField(max_length=36, unique=True)
    def __str__(self):
        return "{0}".format(self.username)


class ZetaClient(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    name = models.CharField(max_length=64)
    empresa_id = models.CharField(max_length=64)
    empresa_key = models.CharField(max_length=64)

    def __str__(self):
        return "{0}".format(self.name)
