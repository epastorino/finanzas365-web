from django.contrib import admin

# Register your models here.
from .models import Client, ZetaClient

admin.site.register(Client)
admin.site.register(ZetaClient)