import os

from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse

from uuid import uuid4

from .models import Client, ZetaClient
from .forms import ClientForm, ZetaClientForm


def check_superuser(user):
    return user.is_superuser

# Create your views here.
@user_passes_test(check_superuser)
@login_required
def client_index(request):
    clients_list = Client.objects.order_by('-username')
    context = {'clients_list':clients_list}
    return render(request, 'clients/clients_index.html', context)


@user_passes_test(check_superuser)
@login_required
def client_detail(request, client_id):
    client = get_object_or_404(Client, pk=client_id)
    try:
        zetaclient = client.zetaclient_set.all()[0]
    except:
        zetaclient = None
    context = {'client':client, 'zetaclient':zetaclient, 'f365_url':os.environ.get('F365_HOSTNAME')}
    return render(request, 'clients/clients_detail.html', context)


@user_passes_test(check_superuser)
@login_required
def client_new(request):
    if request.method == 'POST':
        form = ClientForm(request.POST)
        if form.is_valid():
            client = form.save(commit=False)
            client.set_password(client.password)
            client.key = str(uuid4())
            client.save()
            return redirect('client_index')
    else:
        form = ClientForm()
    return render(request, 'clients/clients_new.html', {'form':form})


@user_passes_test(check_superuser)
@login_required
def client_update(request, client_id):
    client = get_object_or_404(Client, pk=client_id)
    if request.method == 'POST':
        form = ClientForm(request.POST, instance=client)
        if form.is_valid():
            client = form.save(commit=False)
            client.set_password(client.password)
            client.save()
            return redirect('client_detail', client_id=client.id)
    else:
        form = ClientForm(instance=client)
        form.helper.form_action = 'update'
    return render(request, 'clients/clients_update.html', {'form':form})


@user_passes_test(check_superuser)
@login_required
def zetaclient_index(request):
    zclients_list = ZetaClient.objects.all()
    context = {'zclients_list':zclients_list}
    return render(request, 'clients/zclients_index.html', context)


@user_passes_test(check_superuser)
@login_required
def zetaclient_detail(request, zetaclient_id):
    zetaclient = get_object_or_404(ZetaClient, pk=zetaclient_id)
    return render(request, 'clients/zclients_detail.html', {'zclient':zetaclient})


@user_passes_test(check_superuser)
@login_required
def zetaclient_new(request, client_id=None):
    if request.method == 'POST':
        form = ZetaClientForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('zetaclient_index')
    else:
        client = Client.objects.get(id=client_id)
        if client:
            form = ZetaClientForm(initial={'client':client})
        else:
            form = ZetaClientForm()
    return render(request, 'clients/zclients_new.html', {'form':form})

@user_passes_test(check_superuser)
@login_required
def zetaclient_update(request, zetaclient_id):
    zetaclient = get_object_or_404(ZetaClient, pk=zetaclient_id)
    if request.method == 'POST':
        form = ZetaClientForm(request.POST, instance=zetaclient)
        if form.is_valid():
            zetaclient = form.save(commit=False)
            zetaclient.save()
            return redirect('zetaclient_detail', zetaclient_id=zetaclient.id)
    else:
        form = ZetaClientForm(instance=zetaclient)
        form.helper.form_action = 'update'
    return render(request, 'clients/zclients_update.html', {'form':form})
