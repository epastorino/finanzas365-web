from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.client_index, name='client_index'),
	url(r'^new/$', views.client_new, name='client_new'),
	url(r'^(?P<client_id>[0-9]+)/$', views.client_detail, name='client_detail'),
	url(r'^(?P<client_id>[0-9]+)/zeta/new$', views.zetaclient_new, name='client_zetaclient_new'),
	url(r'^(?P<client_id>[0-9]+)/update$', views.client_update, name='client_update'),
	url(r'^zeta/$', views.zetaclient_index, name='zetaclient_index'),
	url(r'^zeta/new/$', views.zetaclient_new, name='zetaclient_new'),
	url(r'^zeta/(?P<zetaclient_id>[0-9]+)/$', views.zetaclient_detail, name='zetaclient_detail'),
	url(r'^zeta/(?P<zetaclient_id>[0-9]+)/update$', views.zetaclient_update, name='zetaclient_update'),
]
