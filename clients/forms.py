from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from .models import Client, ZetaClient

class ClientForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		super(ClientForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.template_pack = 'bootstrap3'
		self.helper.form_id = 'id-client-form'
		self.helper.form_method = 'POST'
		self.helper.form_action = 'client_new'
		self.helper.form_class = 'form-horizontal'
		self.helper.label_class = 'col-md-2'
		self.helper.field_class = 'col-md-8'
		self.helper.help_text_inline = False
		self.helper.add_input(Submit('submit', 'Guardar'))

	class Meta:
		model = Client
		fields = ['username','password']
		widgets = {
			'username': forms.TextInput(),
			'password': forms.PasswordInput(),
		}
		labels = {
			'username': 'Nombre',
		}
		help_texts = {
			'username' : None
		}


class ZetaClientForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		super(ZetaClientForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.template_pack = 'bootstrap3'
		self.helper.form_id = 'id-client-form'
		self.helper.form_method = 'POST'
		self.helper.form_action = 'zetaclient_new'
		self.helper.form_class = 'form-horizontal'
		self.helper.label_class = 'col-md-2'
		self.helper.field_class = 'col-md-8'
		self.helper.help_text_inline = False
		self.helper.add_input(Submit('submit', 'Guardar'))

	class Meta:
		model = ZetaClient
		fields = ['name','empresa_id', 'empresa_key','client']
		widgets = {
			'name': forms.TextInput(),
			'empresa_id': forms.TextInput(),
			'empresa_key': forms.TextInput(),
		}
		labels = {
			'name': 'Descripcion',
			'client': 'Cliente F365'
		}

