from finanzas365_web.settings.common import *

DEBUG = True
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'f365_dev',
        'USER': 'f365_dev',
        'PASSWORD': 'finanzas365123',
        'HOST': '151.80.142.246',
        'PORT': '3306'
    }
}
ALLOWED_HOSTS = ['151.80.142.246', '127.0.0.1']
