from finanzas365_web.settings.common import *

DEBUG = False
#SECURE_CONTENT_TYPE_NOSNIFF = True
#SECURE_BROWSER_XSS_FILTER = True
#SESSION_COOKIE_SECURE = True
#CSRF_COOKIE_SECURE = True
#X_FRAME_OPTIONS = 'DENY'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'OPTIONS': {
            'read_default_file': '/etc/finanzas365/db.conf',
        },
    }
}

ALLOWED_HOSTS = ['svrfin365app01.eastus2.cloudapp.azure.com', '127.0.0.1']
